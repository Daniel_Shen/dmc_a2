//detect edages of an image 
//erosion and dilation to refine the processed image
//retangle out the saliency.
function edageDect(){
	var canvasE = document.getElementById('canvasE');
	var ctxE = document.getElementById('canvasE').getContext('2d');

	var input;
	try{
		 input=ctxE.getImageData(0, 0, canvasE.width, canvasE.height);
	}catch(e){
		netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
		 input=ctxE.getImageData(0, 0, canvasE.width, canvasE.height);
	}
	var result=erosionImage(dilationImage(edages(input)));
	result=erosionImage(dilationImage(result));
	//result=refineEdages(result);
	result=erosionImage(erosionImage(result));
	ctxE.putImageData(result,0,0);
	rectE=rect("yellow",result,canvasE);
	//alert(rectE);
	
}

//copy image data from the orginal image 
function getImage(){
	var canvasE = document.getElementById('canvasE');
	var ctxE = document.getElementById('canvasE').getContext('2d');
	var InputImage;
	try{
		 InputImage=ctxE.getImageData(0, 0, canvasE.width, canvasE.height);
	}catch(e){
		netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
		 InputImage=ctxE.getImageData(0, 0, canvasE.width, canvasE.height);
	}
	return InputImage;
}



//find the deages of an image
//return the processed image data.
function edages(){
			
			var InputImage=getImage();
			var dataCopy = getImage().data;

			var c = -1/8;
			var kernel = [
				[c, 	c, 	c],
				[c, 	1, 	c],
				[c, 	c, 	c]
			];

			weight = 1/c;

			var w = InputImage.width;
			var h = InputImage.height;

			var w4 = w*4;
			var y = h;
			do {
				var offsetY = (y-1)*w4;

				var nextY = (y == h) ? y - 1 : y;
				var prevY = (y == 1) ? 0 : y-2;

				var offsetYPrev = prevY*w*4;
				var offsetYNext = nextY*w*4;

				var x = w;
				do {
					var offset = offsetY + (x*4-4);

					var offsetPrev = offsetYPrev + ((x == 1) ? 0 : x-2) * 4;
					var offsetNext = offsetYNext + ((x == w) ? x-1 : x) * 4;
	
					var r = ((dataCopy[offsetPrev-4]
						+ dataCopy[offsetPrev]
						+ dataCopy[offsetPrev+4]
						+ dataCopy[offset-4]
						+ dataCopy[offset+4]
						+ dataCopy[offsetNext-4]
						+ dataCopy[offsetNext]
						+ dataCopy[offsetNext+4]) * c
						+ dataCopy[offset]
						) 
						* weight;
	
					var g = ((dataCopy[offsetPrev-3]
						+ dataCopy[offsetPrev+1]
						+ dataCopy[offsetPrev+5]
						+ dataCopy[offset-3]
						+ dataCopy[offset+5]
						+ dataCopy[offsetNext-3]
						+ dataCopy[offsetNext+1]
						+ dataCopy[offsetNext+5]) * c
						+ dataCopy[offset+1])
						* weight;
	
					var b = ((dataCopy[offsetPrev-2]
						+ dataCopy[offsetPrev+2]
						+ dataCopy[offsetPrev+6]
						+ dataCopy[offset-2]
						+ dataCopy[offset+6]
						+ dataCopy[offsetNext-2]
						+ dataCopy[offsetNext+2]
						+ dataCopy[offsetNext+6]) * c
						+ dataCopy[offset+2])
						* weight;


						var brightness = (r*0.3 + g*0.59 + b*0.11)||0;
						if (brightness < 0 ) brightness = 0;
						if (brightness > 255 ) brightness = 255;
						r = g = b = brightness;


					InputImage.data[offset] = r;
					InputImage.data[offset+1] = g;
					InputImage.data[offset+2] = b;

				} while (--x);
			} while (--y);
			
		return InputImage;
}
