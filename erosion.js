//erosion for binary image.
//foreground=white=255;
//background=black=0;

function erosion(x,y,filterSize,img)
{			
		//alert(img.width);
		var offset = Math.floor(filterSize / 2);
		for (var i = 0; i < filterSize; i++){
			for (var j = 0; j < filterSize; j++){
			// What pixel are we testing
			var xloc = x+i-offset;
			var yloc = y+j-offset;
			// Is it in range?
				if (xloc >= 0 && xloc <= img.width-filterSize
		 		&& yloc >= 0 && yloc <= img.height-filterSize) {
					var loc = (xloc + (img.width)*yloc)*4;
					if(img.data[loc]==0)
						{
							return 0;
						}
		  		}
			}
		}
		return 255;
}

//InputImage:ImageData that would be processed.
//ctx: context which shares the same image size with  orginal ImageData,
//     which is amied to copy the image structure of the  orginal.
//InputImage: 
//output: erosed ImageData.
function erosionImage(InputImage,ctx){
	var width, height;
	width=InputImage.width;
	height=InputImage.height;
	var output=ctx.createImageData(width, height);
	
	for (var x = 0; x < width; x++) {
	   for (var y = 0; y < height; y++ ) {
	        //gray=InputImage.data[4*(x+y*width)]*0.3+InputImage.data[4*(x+y*width)+1]*0.59+InputImage.data[4*(x+y*width)+2]*0.11;
	   		//if(gray>=100){
	   		if(InputImage.data[4*(x+y*width)]==255){
	   			var c = erosion(x,y,3,InputImage);
	   			output.data[4*(x+y*width)]=output.data[4*(x+y*width)+1]=output.data[4*(x+y*width)+2]=c	   			
	   			output.data[4*(x+y*width)+3]=255;
	   		}
	   		else{
	   			output.data[4*(x+y*width)]  =InputImage.data[4*(x+y*width)];
	   			output.data[4*(x+y*width)+1]=InputImage.data[4*(x+y*width)+1];
	   			output.data[4*(x+y*width)+2]=InputImage.data[4*(x+y*width)+2];
	   			output.data[4*(x+y*width)+3]=InputImage.data[4*(x+y*width)+3];
	   		}
	     }
	}
	return output;
}