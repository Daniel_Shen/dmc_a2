//x,y, width, height
function pick(ImageDataA, rectA){
	var widthA,heightA,perA, objectsA;
	widthA=rectA[2];
	heightA=rectA[3];
	objectsA=0;
	for (var x = rectA[0]; x < rectA[0]+widthA; x++) {
	   for (var y = rectA[1]; y < rectA[1]+heightA; y++ ) {
	   		var loc=4*(x+y*ImageDataA.width);
	   		if(ImageDataA.data[loc]==255)
	   		{
	   			objectsA++;
	   		}
	   }
	} 
	perA=objectsA/(widthA*heightA);
	return perA;
}