function threahold(threahold, grayhis){
  var lessTSum=0;
  var lessTAvg=0;
  var lessSum=0;
    for(var i=0;i<=threahold;i++){
  	lessTSum=lessTSum+grayhis[i]*i;
  	lessSum+=grayhis[i];
  }
  
  lessTAvg=Math.round(lessTSum/lessSum);
  
  var greaterTSum=0;
  var greaterTAvg=0;
  var greaterSum=0;
  for(var i=threahold;i<grayhis.length;i++){
  	greaterTSum=greaterTSum+grayhis[i]*i;
  	greaterSum+=grayhis[i];
  }
  greaterTAvg=Math.round(greaterTSum/greaterSum);
  
  var newThreahold=Math.round((lessTAvg+greaterTAvg)/2);
  return newThreahold;
}

//initialize the arry with default value;
function array256(default_value) {
  arr = [];
  for (var i=0; i<256; i++) { arr[i] = default_value; }
  return arr;
}

//threahold selection
function threaholdselection(T, grayhis){	
	var newThreahold=threahold(T, grayhis);
	try{
	
		while(newThreahold!=T)
			{
				newThreahold=threahold(newThreahold, grayhis);
				T=newThreahold;
				
			}
		}catch(err)
		{
			alert("threadselect error");
		}
	return newThreahold;
   }

/** convert to grayscale image 
	input:context where the original ImageDataw is from and need to be processed.
	output:
	ImageData: another copy of ImageData modified.
**/
function threholding(InputImage, ctx){
try{
	 var output, w,h,inputdata,outputdata;
	 var input;
	 
	 var grayscale=array256(0);
	 var grayavg=0;

	 //var ctx=canvas.getContext('2d');
	 //var canvas = document.getElementById(canvas);
	 //var ctx = document.getElementById(canvas).getContext('2d');
	 //var canvasT = document.getElementById('canvasT');
	 //var ctxT = document.getElementById('canvasT').getContext('2d');
	 //var imageInput=document.getElementById('image').value;		
	 //ctxT = document.getElementById('canvasT').getContext('2d');
	
	 //try{
	 //	input = ctx.getImageData(0, 0, width, height);
	 //}catch(e){
	 //	netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
	 //	input = ctx.getImageData(0, 0, width, height);
	 // }
	
	 w=InputImage.width;
	 h=InputImage.height;
	 input = InputImage;
	 output = ctx.createImageData(w, h);

	 inputdata=input.data;
	 outputdata= output.data;
	 
	 var garysum=0;
	 for(var i=0;i<w*h;i++){
		//get gray value;
		var gray = inputdata[4*i]*0.3+inputdata[4*i+1]*0.59+inputdata[4*i+2]*0.11;
		
		grayscale[gray]+=1;
		garysum+=gray;
	 }
	 
	 //get final threahold
	 grayavg=Math.round(garysum/(w*h));
	 var finalThreahold=threaholdselection(grayavg,grayscale);
	 
	 //filter pixel with threahold.
	 for(var i=0;i<w*h;i++){
	 	var gray = inputdata[4*i]*0.3+inputdata[4*i+1]*0.59+inputdata[4*i+2]*0.11;
	 	if(gray>finalThreahold){
	 	//backgroud=0;
			outputdata[4*i]=outputdata[4*i+1]=outputdata[4*i+2]=0;
	 	}else{
	 	//foreground=255;
	 			outputdata[4*i]=outputdata[4*i+1]=outputdata[4*i+2]=255;
	 	}
	 		
	 	outputdata[4*i+3]=inputdata[4*i+3];
	 }
	 
	 //var result=erosionImage(erosionImage(input));
	 //ctxT.putImageData(result,0,0);
	 //rectT=rect("red",result,canvasT);
	 return output;
	 
	
}
	catch(err)
	{
		return null;
	}
		
}
